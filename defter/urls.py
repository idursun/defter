from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.views.generic import DetailView, UpdateView, CreateView
from core.forms import CompanyForm
from core.models import CompanyLine, Company
from core.views import CompanyListView, CompanyDetailView, CompanyLineCreateView, \
    UserCreateView, CompanyLineUpdateView, CompanyListOverlimitView, CompanyUpdateView, CompanyCreateView, \
    CompanyQueryView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', login_required(CompanyListView.as_view()), name='home'),
    url(r'^company/list/overlimit$', login_required(CompanyListOverlimitView.as_view()), name='company-list-overlimit'),
    url(r'^company/list/(?P<capital>.*)$', login_required(CompanyListView.as_view()), name='company-list-letter'),
    url(r'^company/query/$', login_required(CompanyQueryView.as_view()), name='company-query'),
    url(r'^company/add/$', login_required(CompanyCreateView.as_view()), name='company-create'),
    url(r'^company/(?P<pk>\d+)/edit', login_required(CompanyUpdateView.as_view(model=Company, form_class=CompanyForm)), name='company-edit'),
    url(r'^company/(?P<pk>\d+)$', login_required(CompanyDetailView.as_view()), name='company-detail'),
    url(r'^company/line/(?P<pk>\d+)/add', login_required(CompanyLineCreateView.as_view()), name='company-line-create'),
    url(r'^company/line/(?P<pk>\d+)/edit', login_required(CompanyLineUpdateView.as_view()), name='company-line-edit'),
    url(r'^company/line/(?P<pk>\d+)$', login_required(DetailView.as_view(model=CompanyLine, context_object_name='line')), name='company-line-detail'),

    url(r'^accounts/create/$', login_required(UserCreateView.as_view()), name='user-create'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}, name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    # url(r'^defter/', include('defter.foo.urls')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
