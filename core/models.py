# coding=utf-8
from django.core.urlresolvers import reverse
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    phone = models.CharField(max_length=128)

    def __unicode__(self):
        return u'%s' % (self.user.username,)


class Company(models.Model):
    owner = models.ForeignKey(UserProfile, blank=True, related_name='owned_companies')
    name = models.CharField(max_length=255, verbose_name=u'Şirket adı')
    address = models.TextField(max_length=512, null=True, blank=True, verbose_name=u'Adres')
    phone = models.CharField(max_length=128, null=True, blank=True, verbose_name=u'Şirket telefonu')
    limit = models.IntegerField(null=True, blank=True, verbose_name=u'Uyarı limiti')
    vergidairesi = models.CharField(null=True, blank=True, max_length=128, verbose_name=u'Vergi Dairesi')
    vergino = models.CharField(max_length=20, null=True, blank=True, verbose_name=u'Vergi Numarası')
    fax = models.CharField(max_length=128, null=True, blank=True,verbose_name=u'Fax')
    email = models.EmailField(blank=True, null=True)
    def get_absolute_url(self):
        return reverse('company-detail', kwargs={'pk': self.pk})

    def __unicode__(self):
        return self.name

TIP_CHOICES = (
    ('N', 'Nakit',),
    ('S', 'Senet',),
    ('C', 'Çek',),
    ('P', 'Pos',),
)

class CompanyLine(models.Model):
    is_cancelled = models.BooleanField(default=False, blank=True, verbose_name=u'İptal Mi')
    company = models.ForeignKey(Company, related_name='lines', verbose_name=u'Şirket')
    borc = models.IntegerField(null=True, blank=True, default=0, verbose_name=u'Borç')
    alacak = models.IntegerField(null=True, blank=True, default=0, verbose_name=u'Alacak')
    note = models.CharField(max_length=1024, blank=True, null=True, verbose_name=u'Not')
    tip = models.CharField(max_length=20, verbose_name=u'Tip', default='N', choices=TIP_CHOICES)
    created_at = models.DateField(verbose_name=u'Oluşturulma Tarihi')
    created_by = models.ForeignKey(UserProfile, related_name='created_by', verbose_name=u'Oluşturan')
    modified_at = models.DateTimeField(blank=True, null=True, verbose_name=u'Değiştirilme Tarihi')
    modified_by = models.ForeignKey(UserProfile, null=True, blank=True, related_name='modified_by', verbose_name=u'Değiştiren')

    def get_absolute_url(self):
        return reverse('company-line-detail', kwargs={'pk':self.pk})

    def __unicode__(self):
        return u"borc: %d, alacak: %d" %(self.borc, self.alacak,)


def create_user_profile(sender, instance, created, **kwargs):
    """Create the UserProfile when a new User is saved"""
    if created:
        profile = UserProfile()
        profile.user = instance
        profile.save()

post_save.connect(create_user_profile, sender=User)
