# coding=utf-8
from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.core.paginator import Paginator, EmptyPage
from django.core.urlresolvers import reverse_lazy
from django.db.models import Sum, Q
from django.views.generic import TemplateView, CreateView, UpdateView
from core.forms import CompanyLineForm, CompanyForm, UserCreateForm
from core.models import Company, CompanyLine, UserProfile

class CompanyListView(TemplateView):
    template_name = 'core/company_list.html'

    def get_context_data(self, **kwargs):
        userprofile = self.request.user.get_profile()
        companies = userprofile.owned_companies.annotate(sum_borc=Sum('lines__borc'), sum_alacak=Sum('lines__alacak'))
        if 'capital' in kwargs:
            capital = kwargs.pop('capital')
            q = None
            letters = u"0123456789"
            if capital == '09':
                letters = u"0123456789"
            if capital == 'AD':
                letters = u"ABCÇçD"
            if capital == 'EH':
                letters = u"EFGH"
            if capital == 'IL':
                letters = u"IİiJKL"
            if capital == 'MP':
                letters = u"MNOÖP"
            if capital == 'RT':
                letters = u"RSŞşT"
            if capital == 'UZ':
                letters = u"UÜüVYXZ"

            for letter in letters:
                if q is None:
                    q = Q(name__istartswith=letter)
                else:
                    q |= Q(name__istartswith=letter)

            companies = companies.filter(q)
        context = super(CompanyListView, self).get_context_data(**kwargs)
        context.update({
            'companies': companies,
            'letters': [
                ("09", "0-9"),
                ("AD", "ABCÇD"),
                ("EH", "EFGĞH"),
                ("IL", "IİJKL"),
                ("MP", "MNOÖP"),
                ("RT", "RSŞT"),
                ("UZ", "UÜVYXZ"),
            ],
        })
        return context


class CompanyListOverlimitView(TemplateView):
    template_name = 'core/company_list.html'

    def get_context_data(self, **kwargs):
        userprofile = self.request.user.get_profile()
        companies = userprofile.owned_companies.annotate(sum_borc=Sum('lines__borc'), sum_alacak=Sum('lines__alacak'))
        for company in companies:
            if company.sum_borc is None:
                company.sum_borc = 0
            if company.sum_alacak is None:
                company.sum_alacak = 0
        over_limit = [company for company in companies if
                      not company.limit is None and company.limit < company.sum_borc - company.sum_alacak]
        context = super(CompanyListOverlimitView, self).get_context_data(**kwargs)
        context.update({
            'companies': over_limit,
        })
        return context


class CompanyCreateView(CreateView):
    model = Company
    form_class = CompanyForm

    def form_valid(self, form):
        form.instance.owner = self.request.user.get_profile()
        form.instance.limit = form.cleaned_data['limit_tam'] * 100 + form.cleaned_data['limit_kusur']
        return super(CompanyCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('company-detail', args=(self.object.pk,))


class CompanyUpdateView(UpdateView):
    model = Company
    form_class = CompanyForm

    def form_valid(self, form):
        form.instance.limit = form.cleaned_data['limit_tam'] * 100 + form.cleaned_data['limit_kusur']
        return super(CompanyUpdateView, self).form_valid(form)

    def get_initial(self):
        initial = super(CompanyUpdateView, self).get_initial()
        if self.object.limit is None:
            self.object.limit = 0
        initial['limit_tam'] = int(self.object.limit / 100)
        initial['limit_kusur'] = int(self.object.limit % 100)
        return initial


class CompanyDetailView(TemplateView):
    template_name = 'core/company_detail.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyDetailView, self).get_context_data(**kwargs)
        curr_page_no = 1

        get_queries = self.request.GET.copy()
        if 'page' in get_queries:
            curr_page_no = int(get_queries['page'])
            del get_queries['page']

        company = Company.objects.get(pk=kwargs.pop('pk') or 1)
        lines = CompanyLine.objects.filter(company=company)

        start_date = None
        end_date = None

        if 'start_date' in self.request.GET:
            try:
                start_date = datetime.strptime(self.request.GET.get('start_date'), "%d/%m/%Y")
                lines = lines.filter(created_at__gte=start_date)
            except ValueError:
                pass
        if 'end_date' in self.request.GET:
            try:
                end_date = datetime.strptime(self.request.GET.get('end_date'), "%d/%m/%Y")
                lines = lines.filter(created_at__lte=end_date)
            except ValueError:
                pass

        lines = lines.order_by('created_at')
        paginator = Paginator(lines, 25)
        next_page_no = None
        prev_page_no = None
        try:
            page = paginator.page(curr_page_no)
        except EmptyPage:
            curr_page_no = 1
            page = paginator.page(1)

        if page.has_next():
            next_page_no = curr_page_no + 1

        if page.has_previous():
            prev_page_no = curr_page_no - 1

        total = 0
        for i, line in enumerate(page):
            line.no = (curr_page_no - 1) * 10 + i + 1
            total += line.borc - line.alacak
            line.total = total

        context.update({
            'paginator': paginator,
            'company': company,
            'curr_page_no': curr_page_no,
            'prev_page_no': prev_page_no,
            'next_page_no': next_page_no,
            'lines': page,
            'start_date': start_date,
            'end_date': end_date,
            'queries': get_queries
        })
        return context


class CompanyLineCreateView(CreateView):
    model = CompanyLine
    form_class = CompanyLineForm

    def get_initial(self):
        initial = super(CompanyLineCreateView, self).get_initial()
        initial['created_at'] = datetime.now()
        return initial

    def get_context_data(self, **kwargs):
        context = super(CompanyLineCreateView, self).get_context_data(**kwargs)
        context.update({
            'company': Company.objects.get(pk=self.kwargs.pop('pk'))
        })
        return context


    def form_valid(self, form):
        userprofile, result = UserProfile.objects.get_or_create(user=self.request.user)
        form.instance.company = Company.objects.get(pk=self.kwargs.pop('pk'))
        form.instance.created_by = userprofile
        form.instance.borc = form.cleaned_data['borc_tam'] * 100 + form.cleaned_data['borc_kusur']
        form.instance.alacak = form.cleaned_data['alacak_tam'] * 100 + form.cleaned_data['alacak_kusur']
        form.instance.created_at = form.cleaned_data['created_at']
        return super(CompanyLineCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('company-detail', args=(self.object.company_id,))


class CompanyLineUpdateView(UpdateView):
    model = CompanyLine
    form_class = CompanyLineForm

    def get_initial(self):
        initial = super(CompanyLineUpdateView, self).get_initial()
        initial['borc_tam'] = int(self.object.borc / 100)
        initial['borc_kusur'] = int(self.object.borc % 100)
        initial['alacak_tam'] = int(self.object.alacak / 100)
        initial['alacak_kusur'] = int(self.object.alacak % 100)
        initial['created_at'] = self.object.created_at
        return initial

    def get_context_data(self, **kwargs):
        context = super(CompanyLineUpdateView, self).get_context_data(**kwargs)
        context.update({
            'company': Company.objects.get(pk=self.object.company_id)
        })
        return context

    def form_valid(self, form):
        self.object.modified_by = self.request.user.get_profile()
        self.object.modified_at = datetime.now()
        self.object.borc = form.cleaned_data['borc_tam'] * 100 + form.cleaned_data['borc_kusur']
        self.object.alacak = form.cleaned_data['alacak_tam'] * 100 + form.cleaned_data['alacak_kusur']
        return super(CompanyLineUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('company-detail', args=(self.object.company_id,))


class UserCreateView(CreateView):
    form_class = UserCreateForm
    success_url = reverse_lazy('home')
    template_name = 'core/user_form.html'

class CompanyQueryView(TemplateView):
    template_name = 'core/company_query.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyQueryView, self).get_context_data(**kwargs)
        start_date = datetime.now() - relativedelta(months=1)
        end_date = datetime.now()
        if 'start_date' in self.request.GET:
            start_date = datetime.strptime(self.request.GET.get('start_date'), "%d/%m/%Y")
        if 'end_date' in self.request.GET:
            end_date = datetime.strptime(self.request.GET.get('end_date'), "%d/%m/%Y")

        query = list(CompanyLine.objects.filter(created_at__gte=start_date, created_at__lte=end_date).values(
            'company__owner').annotate(sum_borc=Sum('borc'), sum_alacak=Sum('alacak')))
        for line in query:
            line['owner'] = UserProfile.objects.get(pk=line['company__owner'])
        context.update({
            'lines': query,
            'query': u"?start_date={0}&end_date={1}".format(start_date.strftime("%d/%m/%Y").encode(),
                end_date.strftime("%d/%m/%Y").encode()),
            'start_date': start_date,
            'end_date': end_date
        })
        return context
