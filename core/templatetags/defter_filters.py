from django import template

register = template.Library()

@register.filter
def tl(number):
    if number is None or number == 0:
        return "0,00 TL"
    s = str(number)
    while len(s) < 3:
        s = "0" + s

    return u"%s,%s TL" % (s[:-2], s[-2:])
